mod bindings {
    windows::include_bindings!();
}

use std::env;

use bindings::{
    Windows::Win32::Debug::{MiniDumpWriteDump, MINIDUMP_TYPE},
    Windows::Win32::FileSystem::{
        CreateFileA, FILE_ACCESS_FLAGS, FILE_CREATION_DISPOSITION, FILE_FLAGS_AND_ATTRIBUTES,
        FILE_SHARE_MODE,
    },
    Windows::Win32::SystemServices::{OpenProcess, PROCESS_ACCESS_RIGHTS},
};

fn main() -> windows::Result<()> {
    let args: Vec<String> = env::args().collect();

    if args.len() < 2 {
        panic!("Expected the process PID!");
    }

    let process_pid = args[1].parse::<u32>().unwrap();

    unsafe {      
        let out = CreateFileA(
            "process_d.log",
            FILE_ACCESS_FLAGS::FILE_GENERIC_READ | FILE_ACCESS_FLAGS::FILE_GENERIC_WRITE,
            FILE_SHARE_MODE::FILE_SHARE_NONE,
            std::ptr::null_mut(),
            FILE_CREATION_DISPOSITION::CREATE_ALWAYS,
            FILE_FLAGS_AND_ATTRIBUTES::FILE_ATTRIBUTE_NORMAL,
            None,
        );

        let h_proc = OpenProcess(PROCESS_ACCESS_RIGHTS::PROCESS_ALL_ACCESS, None, process_pid);

        let _is_dumped = MiniDumpWriteDump(
            h_proc,
            process_pid,
            out,
            MINIDUMP_TYPE::MiniDumpWithFullMemory,
            std::ptr::null_mut(),
            std::ptr::null_mut(),
            std::ptr::null_mut(),
        );
        if _is_dumped.as_bool() {
            println!("[+] dump successful.");
        }
    }

    Ok(())
}
