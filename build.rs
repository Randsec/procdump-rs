fn main() {
    windows::build!(
        Windows::Win32::Debug::{
            MINIDUMP_TYPE,MiniDumpWriteDump
        },
        Windows::Win32::FileSystem::{
            FILE_CREATION_DISPOSITION,FILE_FLAGS_AND_ATTRIBUTES,FILE_SHARE_MODE, CreateFileA, FILE_ACCESS_FLAGS
        },
        Windows::Win32::SystemServices::{
            OpenProcess,PROCESS_ACCESS_RIGHTS
        }
    );
}
