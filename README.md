# Dump process memory using rust
A simple POC using [windows's api bindings](https://crates.io/crates/windows).

## compile
`cargo build --release`

## usage
`dump-rs.exe <pid>`

File __d_process.log__ will be generated.

## example
![](example_lsass.png)

## references
https://github.com/microsoft/windows-rs

https://microsoft.github.io/windows-docs-rs/doc/bindings/index.html
